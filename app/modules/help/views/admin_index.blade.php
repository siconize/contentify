<p>Welcome to the first beta version of Contentify! Since this is a very early version of Contentify, some content is still missing. For example, the documentation ist still on our to-do-list.</p>

<p>But that does not mean you are lost. If you have any questions, find any bugs or just want to get in touch: Simply send a mail to <span style="border-bottom: 1px dotted silver">{{ HTML::mailto('support@contentify.org') }}</span></p>

<h2>I've spotted a Bug!</h2>

<p>Nice one! Contentify is still in an early stage of development. Any kind of software that is in beta state is the most natural habitation of bugs. So, if you find a bug, please let us know! Knowing where they life is the first step to hunt them down. If you want to help us, then please send us a mail. The more information that describes the bug you provide, the faster we will find it. Screeenshots are very welcome. Also note that there's a <span style="border-bottom: 1px dotted silver">{{ link_to('admin/config/log', 'log file') }}</span>.</p>